from importlib import metadata
from pathlib import Path

from kart import Kart, mappers, miners, renderers
from kart.ext import documentation
from kart.utils import date_to_string


class BootstrapMapper(mappers.Mapper):
    def map(self, config, site):
        return {
            "bootstrap": {
                "url": "/static/css/bootstrap.min.css",
                "renderer": "bootstrap_renderer",
            },
            "bootstrap_map": {
                "url": "/static/css/bootstrap.min.css.map",
                "renderer": "bootstrap_renderer",
            },
        }


class BootstrapRenderer(renderers.DefaultFileRenderer):
    def __init__(self):
        self.name = "bootstrap_renderer"
        self.content_type = "text/css"

    def render_single(self, page, config, site, sitemap):
        theme = site["data"]["theme"]["theme"]
        filename = Path(page["url"]).name
        style = Path("bootswatch", "dist", theme, filename)
        return style.read_text("utf-8")


kart = Kart(build_location="public")

kart.miners = [
    miners.WatchDirectoryMiner(["root", "static", "templates"]),
    miners.DefaultDataMiner(),
    documentation.DefaultDocumentationMiner(),
]

kart.content_modifiers = []

kart.mappers = [
    documentation.DefaultDocumentationMapper(template="default.html"),
    mappers.DefaultSitemapMapper(),
    mappers.DefaultStaticFilesMapper(),
    mappers.DefaultRootDirMapper(),
    BootstrapMapper(),
]

kart.map_modifiers = []

kart.renderers = [
    renderers.DefaultSiteRenderer(
        filters={
            "html": documentation.markdown_to_html,
            "toc": documentation.markdown_to_toc,
            "date_to_string": date_to_string,
        }
    ),
    renderers.DefaultSitemapRenderer(),
    renderers.DefaultStaticFilesRenderer(),
    renderers.DefaultRootDirRenderer(),
    BootstrapRenderer(),
]

kart.config["name"] = "Kart"
kart.config["copyright"] = "© Copyright 2020-2024, Giacomo Caironi"
kart.config["icon"] = "/favicon-32x32.png"
kart.config["version"] = metadata.version(kart.config["name"])
kart.config["repo_url"] = "https://gitlab.com/giacomocaironi/kart"
kart.config["repo_name"] = "giacomocaironi/kart"
kart.config["site_url"] = "https://kart.giacomocaironi.dev"
kart.config["code_highlighting"] = {"style": "material"}

if __name__ == "__main__":
    kart.run()
