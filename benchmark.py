import os
import random
import shutil
import string
import time
from math import ceil, log
from pathlib import Path
from textwrap import dedent

import lorem
from cookiecutter.main import cookiecutter

POSTS_NUM = 1000
PAGE_NUM = 4
TAGS_NUM = 10
PARAGRAPH_COUNT = 5
SENTENCE_COUNT = 30

base_path = Path("kart_benchmark")

shutil.rmtree(base_path, True)

start = time.time()
cookiecutter(
    "cookiecutters/blog",
    extra_context={"project_name": "Kart benchmark"},
    no_input=True,
)

tags = []
for _ in range(TAGS_NUM):
    NAME = "".join(random.choice(string.ascii_lowercase) for _ in range(10))
    file = base_path / "taxonomies" / "tags" / f"{NAME}.md"
    text = f"""
    ---
    name: {NAME}
    ---
    """
    file.write_text(f"{dedent(text).strip()}\n")
    tags.append(NAME)


for _ in range(POSTS_NUM):
    NAME = "".join(random.choice(string.ascii_lowercase) for _ in range(10))
    file = base_path / "collections" / "posts" / f"{NAME}.md"
    post_tags = random.sample(tags, min(ceil(-log(1 - random.random())), TAGS_NUM))
    header = f"""
    title: {NAME}
    date: 2021-07-16
    author: Giacomo Caironi
    description: {lorem.get_sentence(5)}
    tags:\n"""
    for tag in post_tags:
        header += f"        - {tag}\n" ""
    sentences = []
    for _ in range(PARAGRAPH_COUNT):
        TITLE = "".join(random.choice(string.ascii_lowercase) for _ in range(10))
        sentence = lorem.get_sentence(SENTENCE_COUNT)
        sentences.append(f"# {TITLE.capitalize()}\n\n{sentence}\n")
    file.write_text(f"---\n{dedent(header).strip()}\n---\n\n" + "\n".join(sentences))

for _ in range(PAGE_NUM):
    NAME = "".join(random.choice(string.ascii_lowercase) for _ in range(10))
    file = base_path / "pages" / f"{NAME}.md"
    post_tags = random.sample(tags, min(ceil(-log(1 - random.random())), TAGS_NUM))
    header = f"""
    title: {NAME}
    date: 2021-07-16
    author: Giacomo Caironi
    """
    sentences = []
    for _ in range(PARAGRAPH_COUNT):
        TITLE = "".join(random.choice(string.ascii_lowercase) for _ in range(10))
        sentence = lorem.get_sentence(SENTENCE_COUNT)
        sentences.append(f"# {TITLE.capitalize()}\n\n{sentence}\n")
    file.write_text(f"---\n{dedent(header).strip()}\n---\n\n" + "\n".join(sentences))

end = time.time()
print(f"Setup time {(end-start):.4f}s")


from kart_benchmark.main import (  # noqa: E402  pylint: disable=wrong-import-position
    kart,
)

os.chdir(base_path)

start = time.time()
kart.run()
end = time.time()
print(f"Build time {(end-start):.4f}s")
